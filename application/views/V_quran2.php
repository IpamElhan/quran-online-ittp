<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
        <title>AlQuran Online ITTP</title>

        <!-- using online links-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">

        <!-- using local links  -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/all.css');?>">
         <link rel="stylesheet" href="<?php echo base_url('assets/css/border.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.mCustomScrollbar.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/sidebar-themes.css');?>">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/img/favicon.png');?>" />

        <style> 
            
        </style>
    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top " style="background-color: #e3f2fd;">
            <div class="container">
                <a class="navbar-brand" href="#"><b>AlQuran Online<b></a>

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home
                            <span class="sr-only">(current)</span>
                        </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a id="toggle-sidebar" class="nav-link fa fa-bars fa-dark" href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="page-wrapper ice-theme sidebar-bg bg1 toggled">
            <nav id="sidebar" class="sidebar-wrapper">
                <div class="sidebar-content">
                    <!-- sidebar-brand  -->
                    <div class="sidebar-item sidebar-brand">
                        <a href="#">
                            <center>Alquran Online</center>
                        </a>
                    </div>
                    <!-- sidebar-header  -->
                    <div class="sidebar-item sidebar-header d-flex flex-nowrap">
                        <div class="user-pic">
                            <img class="img-responsive img-rounded" src="<?php echo base_url('assets/img/ITTP.png');?>" alt="User picture">
                        </div>
                        <div class="user-info">
                            <span class="user-name">
                            <strong>Institut Teknologi</strong>
                        </span>
                            <span class="user-name">
                            <strong>Telkom Purwokerto</strong>
                        </span>
                        </div>
                    </div>

                    <div class="sidebar-item sidebar-header">
                        <div class="row align-item-between">
                            <div class="col-md-6">
                                <a id="" class="fa fa-home fa-dark" href="#">
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a id="pin-sidebar" class="fa fa-toggle-on fa-dark float-right" href="#">
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- sidebar-search  -->
                    <div class="sidebar-item sidebar-search">
                        <div>
                            <div class="input-group">
                                <input type="text" class="form-control search-menu" placeholder="Search...">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- sidebar-menu  -->                 
<?php
    
    $juz= array(
    1 => 'Juz 1',
    2 => 'Juz 2',
    3 => 'Juz 3',
    4 => 'Juz 4',
    5 => 'Juz 5',
    6 => 'Juz 6',
    7 =>  'Juz 7',
    8 =>  'Juz 8',
    9 =>  'Juz 9',
    10 =>  'Juz 10',
    11 =>  'Juz 11',
    12 =>  'Juz 12',
    13 =>  'Juz 13',
    14 =>  'Juz 14',
    15 =>  'Juz 15',
    16 =>  'Juz 16',
    17 =>  'Juz 17',
    18 =>  'Juz 18',
    19 =>  'Juz 19',
    20 =>  'Juz 20',
    21 =>  'Juz 21',
    22 =>  'Juz 22',
    23 =>  'Juz 23',
    24 =>  'Juz 24',
    25 =>  'Juz 25',
    26 =>  'Juz 26',
    27 =>  'Juz 27',
    28 =>  'Juz 28',
    29 =>  'Juz 29',
    30 =>  'Juz 30',
);

    $ayat = array(
    1 => array('Al-Fatihah', '7'),
    2 => array('Al-Baqarah', '286'),
    3 => array('Ali Imran', '200'),
    4 => array('An-Nisaa', '176'),
    5 => array('Al-Maidah', '120'),
    6 => array('Al-An\'am', '165'),
    7 => array('Al-A\'raf', '206'),
    8 => array('AL-Anfal', '75'),
    9 => array('At-Taubah', '129'),
    10 => array('Yunus', '109'),
    11 => array('Hud', '123'),
    12 => array('Yusuf', '111'),
    13 => array('Ar-Ra\'d', '43'),
    14 => array('Ibrahim', '52'),
    15 => array('Al-Hijr', '99'),
    16 => array('An-Nahl', '128'),
    17 => array('Al-Isra', '111'),
    18 => array('Al-Kahf', '110'),
    19 => array('Maryam', '98'),
    20 => array('Thaha', '135'),
    21 => array('Al-Anbiya', '112'),
    22 => array('Al-Hajj', '78'),
    23 => array('Al-Muminun', '118'),
    24 => array('An-Nur', '64'),
    25 => array('Al-Furqan', '77'),
    26 => array('Asy-Syu\'ara', '227'),
    27 => array('An-Naml', '93'),
    28 => array('Al-Qasas', '88'),
    29 => array('Al-Ankabut', '69'),
    30 => array('Ar-Rum', '60'),
    31 => array('Luqman', '34'),
    32 => array('As-Sajdah', '30'),
    33 => array('Al-Ahzab', '73'),
    34 => array('Saba\'', '54'),
    35 => array('Fatir', '45'),
    36 => array('Ya Sin', '83'),
    37 => array('As-Saffat', '182'),
    38 => array('Sad', '88'),
    39 => array('Az-Zumar', '75'),
    40 => array('Gafir', '85'),
    41 => array('Fussilat', '54'),
    42 => array('Asy-Syura', '53'),
    43 => array('Az-Zukhruf', '89'),
    44 => array('Ad-Dukhan', '59'),
    45 => array('Al-Jasiyah', '37'),
    46 => array('Al-Ahqaf', '35'),
    47 => array('Muhammad', '38'),
    48 => array('Al-Fath', '29'),
    49 => array('Al-Hujurat', '18'),
    50 => array('Qaf', '45'),
    51 => array('Az-Zariyat', '60'),
    52 => array('At-Tur', '49'),
    53 => array('An-Najm', '62'),
    54 => array('Al-Qamar', '55'),
    55 => array('Ar-Rahman', '78'),
    56 => array('Al-Waqi\'ah', '96'),
    57 => array('Al-Hadid', '29'),
    58 => array('Al-Mujadalah', '22'),
    59 => array('Al-Hasyr', '24'),
    60 => array('Al-Mumtahanah', '13'),
    61 => array('As-Saff', '14'),
    62 => array('Al-Jumuah', '11'),
    63 => array('Al-Munafiqun', '11'),
    64 => array('At-Tagabun', '18'),
    65 => array('At-Talaq', '12'),
    66 => array('At-Tahrim', '12'),
    67 => array('Al-Mulk', '30'),
    68 => array('Al-Qalam', '52'),
    69 => array('Al-Haqqah', '52'),
    70 => array('Al-Maarij', '44'),
    71 => array('Nuh', '28'),
    72 => array('Al-Jinn', '28'),
    73 => array('Al-Muzammil', '20'),
    74 => array('Al_muddatstsir', '56'),
    75 => array('Al-Qiyamah', '40'),
    76 => array('Al-Insan', '31'),
    77 => array('Al-Mursalat', '50'),
    78 => array('An-Nabaa', '40'),
    79 => array('An-Nazi\'at', '46'),
    80 => array('Abasa', '42'),
    81 => array('At-Takwir', '29'),
    82 => array('Al-Infitar', '19'),
    83 => array('Al-Muthaffifin', '36'),
    84 => array('Al-Insyiqaq', '25'),
    85 => array('Al-Buruj', '22'),
    86 => array('At-Tariq', '17'),
    87 => array('Al-A\'la', '19'),
    88 => array('Al-Ghasyiyah', '26'),
    89 => array('Al-Fajr', '30'),
    90 => array('Al-Balad', '20'),
    91 => array('Asy-Syams', '15'),
    92 => array('Al-Lail', '21'),
    93 => array('Ad-Duha', '11'),
    94 => array('As-Syarh', '8'),
    95 => array('At-Tin', '8'),
    96 => array('Al-Alaq', '19'),
    97 => array('Al-Qadr', '5'),
    98 => array('AL-Bayyinah', '8'),
    99 => array('Az-Zalzalah', '8'),
    100 => array('Al-Adiyat', '11'),
    101 => array('AL-Qariah', '11'),
    102 => array('At-Takasur', '8'),
    103 => array('Al-Asr', '3'),
    104 => array('AL-Humazah', '9'),
    105 => array('Al-Fil', '5'),
    106 => array('Quraisy', '4'),
    107 => array('Al-Maun', '7'),
    108 => array('Al-Kausar', '3'),
    109 => array('Al-Kafirun', '6'),
    110 => array('An-Nasr', '3'),
    111 => array('Al-Lahab', '5'),
    112 => array('Al-Ikhlas', '4'),
    113 => array('Al-Falaq', '5'),
    114 => array('An-Nas', '6'),
);
?>
                    <div class=" sidebar-item sidebar-menu">
                        <ul>
                            <li class="header-menu">
                                <span>General</span>
                            </li>
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-book"></i>
                                    <span class="menu-text">Berdasarkan Surat</span>
                                    <span class="badge badge-pill badge-danger">   114</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="#">1.  <?php echo ($ayat[1][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">2.  <?php echo ($ayat[2][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">3.  <?php echo ($ayat[3][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">4.  <?php echo ($ayat[4][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">5.  <?php echo ($ayat[5][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">6.  <?php echo ($ayat[6][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">7.  <?php echo ($ayat[7][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">8.  <?php echo ($ayat[8][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">9.  <?php echo ($ayat[9][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">10.  <?php echo ($ayat[10][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">11.  <?php echo ($ayat[11][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">12.  <?php echo ($ayat[12][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">13.  <?php echo ($ayat[13][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">14.  <?php echo ($ayat[14][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">15.  <?php echo ($ayat[15][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">16.  <?php echo ($ayat[16][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">17.  <?php echo ($ayat[17][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">17.  <?php echo ($ayat[17][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">18.  <?php echo ($ayat[18][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">19.  <?php echo ($ayat[19][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">20.  <?php echo ($ayat[20][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">21.  <?php echo ($ayat[21][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">22.  <?php echo ($ayat[22][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">23.  <?php echo ($ayat[23][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">24.  <?php echo ($ayat[24][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">25.  <?php echo ($ayat[25][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">26.  <?php echo ($ayat[26][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">27.  <?php echo ($ayat[27][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">28.  <?php echo ($ayat[28][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">29.  <?php echo ($ayat[29][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">30.  <?php echo ($ayat[30][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">31.  <?php echo ($ayat[31][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">32.  <?php echo ($ayat[32][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">33.  <?php echo ($ayat[33][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">34.  <?php echo ($ayat[34][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">35.  <?php echo ($ayat[35][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">36.  <?php echo ($ayat[36][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">37.  <?php echo ($ayat[37][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">38.  <?php echo ($ayat[38][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">39.  <?php echo ($ayat[39][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">40.  <?php echo ($ayat[40][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">41.  <?php echo ($ayat[41][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">42.  <?php echo ($ayat[42][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">43.  <?php echo ($ayat[43][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">44.  <?php echo ($ayat[44][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">45.  <?php echo ($ayat[45][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">46.  <?php echo ($ayat[46][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">47.  <?php echo ($ayat[47][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">48.  <?php echo ($ayat[48][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">49.  <?php echo ($ayat[49][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">50.  <?php echo ($ayat[50][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">51.  <?php echo ($ayat[51][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">52.  <?php echo ($ayat[52][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">53.  <?php echo ($ayat[53][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">54.  <?php echo ($ayat[54][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">55.  <?php echo ($ayat[55][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">56.  <?php echo ($ayat[56][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">57.  <?php echo ($ayat[57][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">58.  <?php echo ($ayat[58][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">59.  <?php echo ($ayat[59][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">60.  <?php echo ($ayat[60][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">61.  <?php echo ($ayat[61][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">62.  <?php echo ($ayat[62][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">63.  <?php echo ($ayat[63][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">64.  <?php echo ($ayat[64][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">65.  <?php echo ($ayat[65][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">66.  <?php echo ($ayat[66][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">67.  <?php echo ($ayat[67][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">68.  <?php echo ($ayat[68][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">69.  <?php echo ($ayat[69][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">70.  <?php echo ($ayat[70][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">71.  <?php echo ($ayat[71][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">72.  <?php echo ($ayat[72][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">73.  <?php echo ($ayat[73][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">74.  <?php echo ($ayat[74][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">75.  <?php echo ($ayat[75][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">76.  <?php echo ($ayat[76][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">77.  <?php echo ($ayat[77][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">78.  <?php echo ($ayat[78][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">79.  <?php echo ($ayat[79][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">80.  <?php echo ($ayat[80][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">81.  <?php echo ($ayat[81][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">82.  <?php echo ($ayat[82][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">83.  <?php echo ($ayat[83][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">84.  <?php echo ($ayat[84][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">85.  <?php echo ($ayat[85][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">86.  <?php echo ($ayat[86][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">87.  <?php echo ($ayat[87][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">88.  <?php echo ($ayat[88][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">89.  <?php echo ($ayat[89][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">90.  <?php echo ($ayat[90][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">91.  <?php echo ($ayat[91][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">92.  <?php echo ($ayat[92][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">93.  <?php echo ($ayat[93][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">94.  <?php echo ($ayat[94][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">95.  <?php echo ($ayat[95][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">96.  <?php echo ($ayat[96][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">97.  <?php echo ($ayat[97][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">98.  <?php echo ($ayat[98][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">99.  <?php echo ($ayat[99][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">100.  <?php echo ($ayat[100][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">101.  <?php echo ($ayat[101][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">102.  <?php echo ($ayat[102][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">103.  <?php echo ($ayat[103][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">104.  <?php echo ($ayat[104][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">105.  <?php echo ($ayat[105][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">106.  <?php echo ($ayat[106][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">107.  <?php echo ($ayat[107][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">108.  <?php echo ($ayat[108][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">109.  <?php echo ($ayat[109][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">110.  <?php echo ($ayat[110][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">111.  <?php echo ($ayat[111][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">112.  <?php echo ($ayat[112][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">113.  <?php echo ($ayat[113][0]); ?></a>
                                        </li>
                                        <li>
                                            <a href="#">114.  <?php echo ($ayat[114][0]); ?></a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-book"></i>
                                    <span class="menu-text">Berdasarkan Juz </span>
                                    <span class="badge badge-pill badge-danger">   30</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="#"><?php echo ($juz[1]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[2]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[3]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[4]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[5]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[6]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[7]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[8]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[9]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[10]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[11]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[12]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[13]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[14]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[15]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[16]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[17]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[18]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[19]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[20]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[21]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[22]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[23]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[24]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[25]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[26]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[27]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[28]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[29]);?></a>
                                        </li>
                                        <li>
                                            <a href="#"><?php echo ($juz[30]);?></a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="header-menu">
                            <span>Surah</span>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text">1. <?php echo ucfirst($ayat[1][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[1][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">2. <?php echo (ucfirst($ayat[2][0])); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[2][1] ;?></span>
                                </a>
                            </li>

                             <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">3. <?php echo (ucfirst($ayat[3][0])); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[3][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">4. <?php echo ($ayat[4][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[4][1] ;?></span>
                                </a>
                            </li>

                             <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">5. <?php echo (ucfirst($ayat[5][0])); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[5][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">6. <?php echo ($ayat[6][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[6][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">7. <?php echo ($ayat[7][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[7][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">8. <?php echo ($ayat[8][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[8][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">9. <?php echo ($ayat[9][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[9][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">10. <?php echo ($ayat[10][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[10][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">11. <?php echo ($ayat[11][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[11][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">12. <?php echo ($ayat[12][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[12][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">13. <?php echo ($ayat[13][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[13][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">14. <?php echo ($ayat[14][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[14][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">15. <?php echo ($ayat[15][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[15][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">16. <?php echo ($ayat[16][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[16][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">17. <?php echo ($ayat[17][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[17][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">18. <?php echo ($ayat[18][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[18][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">19. <?php echo ($ayat[19][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[19][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">20. <?php echo ($ayat[20][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[20][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">21. <?php echo ($ayat[21][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[21][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">22. <?php echo ($ayat[22][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[22][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">23. <?php echo ($ayat[23][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[23][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">24. <?php echo ($ayat[24][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[24][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">25. <?php echo ($ayat[25][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[25][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">26. <?php echo ($ayat[26][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[26][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">27. <?php echo ($ayat[27][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[27][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">28. <?php echo ($ayat[28][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[28][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">29. <?php echo ($ayat[29][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[29][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">30. <?php echo ($ayat[30][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[30][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">31. <?php echo ($ayat[31][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[31][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">32. <?php echo ($ayat[32][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[32][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">33. <?php echo ($ayat[33][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[33][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">34. <?php echo ($ayat[34][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[34][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">35. <?php echo ($ayat[35][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[35][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">36. <?php echo ($ayat[36][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[36][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">37. <?php echo ($ayat[37][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[37][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">38. <?php echo ($ayat[38][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[38][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">39. <?php echo ($ayat[39][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[39][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">40. <?php echo ($ayat[40][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[40][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">41. <?php echo ($ayat[41][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[41][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">42. <?php echo ($ayat[42][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[42][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">43. <?php echo ($ayat[43][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[43][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">44. <?php echo ($ayat[44][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[44][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">45. <?php echo ($ayat[45][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[45][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">46. <?php echo ($ayat[46][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[46][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">47. <?php echo ($ayat[47][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[47][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">48. <?php echo ($ayat[48][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[48][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">49. <?php echo ($ayat[49][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[49][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">50. <?php echo ($ayat[50][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[50][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">51. <?php echo ($ayat[51][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[51][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">52. <?php echo ($ayat[52][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[52][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">53. <?php echo ($ayat[53][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[53][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">54. <?php echo ($ayat[54][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[54][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">55. <?php echo ($ayat[55][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[55][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">56. <?php echo ($ayat[56][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[56][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">57. <?php echo ($ayat[57][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[57][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">58. <?php echo ($ayat[58][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[58][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">59. <?php echo ($ayat[59][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[59][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">60. <?php echo ($ayat[60][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[60][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">61. <?php echo ($ayat[61][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[61][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">62. <?php echo ($ayat[62][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[62][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">63. <?php echo ($ayat[63][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[63][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">64. <?php echo ($ayat[64][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[64][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">65. <?php echo ($ayat[65][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[65][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">66. <?php echo ($ayat[66][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[66][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">67. <?php echo ($ayat[67][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[67][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">68. <?php echo ($ayat[68][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[68][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">69. <?php echo ($ayat[69][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[69][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">70. <?php echo ($ayat[70][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[70][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">71. <?php echo ($ayat[71][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[71][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">72. <?php echo ($ayat[72][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[72][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">73. <?php echo ($ayat[73][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[73][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">74. <?php echo ($ayat[74][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[74][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">75. <?php echo ($ayat[75][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[75][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">76. <?php echo ($ayat[76][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[76][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">77. <?php echo ($ayat[77][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[77][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">78. <?php echo ($ayat[78][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[78][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">79. <?php echo ($ayat[79][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[79][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">80. <?php echo ($ayat[80][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[80][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">81. <?php echo ($ayat[81][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[81][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">82. <?php echo ($ayat[82][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[82][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">83. <?php echo ($ayat[83][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[83][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">84. <?php echo ($ayat[84][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[84][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">85. <?php echo ($ayat[85][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[85][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">86. <?php echo ($ayat[86][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[86][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">87. <?php echo ($ayat[87][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[87][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">88. <?php echo ($ayat[88][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[88][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">89. <?php echo ($ayat[89][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[89][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">90. <?php echo ($ayat[90][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[90][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">91. <?php echo ($ayat[91][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[91][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">92. <?php echo ($ayat[92][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[92][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">93. <?php echo ($ayat[93][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[93][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">94. <?php echo ($ayat[94][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[94][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">95. <?php echo ($ayat[95][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[95][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">96. <?php echo ($ayat[96][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[96][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">97. <?php echo ($ayat[97][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[97][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">98. <?php echo ($ayat[98][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[98][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">99. <?php echo ($ayat[99][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[99][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">100. <?php echo ($ayat[100][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[100][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">101. <?php echo ($ayat[101][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[101][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">102. <?php echo ($ayat[102][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[102][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">103. <?php echo ($ayat[103][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[103][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">104. <?php echo ($ayat[104][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[104][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">105. <?php echo ($ayat[105][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[105][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">106. <?php echo ($ayat[106][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[106][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">107. <?php echo ($ayat[107][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[107][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">108. <?php echo ($ayat[108][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[108][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">109. <?php echo ($ayat[109][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[109][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">110. <?php echo ($ayat[110][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[110][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">111. <?php echo ($ayat[111][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[111][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">112. <?php echo ($ayat[112][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[112][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">113. <?php echo ($ayat[113][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[113][1] ;?></span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-quran"></i>
                                    <span class="menu-text">114. <?php echo ($ayat[114][0]); ?></span>
                                    <span class="badge badge-pill badge-primary" style="font-size: 10px"><?php echo $ayat[114][1] ;?></span>
                                </a>
                            </li>











                        </ul>   
                    </div>
                    <!-- sidebar-menu  -->
                </div>
                <!-- sidebar-footer  -->
                <div class="sidebar-footer">
                    <div class="dropdown">

                        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="badge badge-pill badge-warning notification">3</span>
                        </a>
                        <div class="dropdown-menu notifications" aria-labelledby="dropdownMenuMessage">
                            <div class="notifications-header">
                                <i class="fa fa-bell"></i> Notifications
                            </div>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">
                                <div class="notification-content">
                                    <div class="icon">
                                        <i class="fas fa-check text-success border border-success"></i>
                                    </div>
                                    <div class="content">
                                        <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                                        <div class="notification-time">
                                            6 minutes ago
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="dropdown-item" href="#">
                                <div class="notification-content">
                                    <div class="icon">
                                        <i class="fas fa-exclamation text-info border border-info"></i>
                                    </div>
                                    <div class="content">
                                        <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                                        <div class="notification-time">
                                            Today
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="dropdown-item" href="#">
                                <div class="notification-content">
                                    <div class="icon">
                                        <i class="fas fa-exclamation-triangle text-warning border border-warning"></i>
                                    </div>
                                    <div class="content">
                                        <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                                        <div class="notification-time">
                                            Yesterday
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-center" href="#">View all notifications</a>
                        </div>
                    </div>
                    <div class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-envelope"></i>
                            <span class="badge badge-pill badge-success notification">7</span>
                        </a>
                        <div class="dropdown-menu messages" aria-labelledby="dropdownMenuMessage">
                            <div class="messages-header">
                                <i class="fa fa-envelope"></i> Messages
                            </div>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">
                                <div class="message-content">
                                    <div class="pic">
                                        <img src="<?php echo base_url('assets/img/user.jpg');?>" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="message-title">
                                            <strong> Jhon doe</strong>
                                        </div>
                                        <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                                    </div>
                                </div>

                            </a>
                            <a class="dropdown-item" href="#">
                                <div class="message-content">
                                    <div class="pic">
                                        <img src="<?php echo base_url('assets/img/user.jpg');?>" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="message-title">
                                            <strong> Jhon doe</strong>
                                        </div>
                                        <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                                    </div>
                                </div>

                            </a>
                            <a class="dropdown-item" href="#">
                                <div class="message-content">
                                    <div class="pic">
                                        <img src="<?php echo base_url('assets/img/user.jpg');?>" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="message-title">
                                            <strong> Jhon doe</strong>
                                        </div>
                                        <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-center" href="#">View all messages</a>

                        </div>
                    </div>
                    <div class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                            <span class="badge-sonar"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuMessage">
                            <a class="dropdown-item" href="#">My profile</a>
                            <a class="dropdown-item" href="#">Help</a>
                            <a class="dropdown-item" href="#">Setting</a>
                        </div>
                    </div>
                    <div>
                        <a href="#">
                            <i class="fa fa-power-off"></i>
                        </a>
                    </div>
                    <div class="pinned-footer">
                        <a href="#">
                            <i class="fas fa-ellipsis-h"></i>
                        </a>
                    </div>
                </div>
            </nav>

            <!-- page-content  -->
            <main class="page-content pt-2 alert-dark">
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid pt-5">

                    <div class="row">

                    </div>

                    <div class="row">
                        <div class="container-fluid" style="background:white url('<?php echo base_url('assets/img/ornamen6.jpg');?>') repeat-x scroll;width: 100%;height:32px;"></div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <h4>Alquran Online ITTP</h4>
                            <p>Quran Online terjemahan perkata,tajwid,latin dan asbabun nuzul</p>
                        </div>
                        <div class="form-group col-md-2 float-right">
                            <h4 style="text-align: right;"></h4>
                        </div>
                        <div class="form-group col-md-2 float-right">
                            <div class="row">
                                <p style="text-align: left;">JUMLAH SURAH</p>
                            </div>
                            <div class="row">
                                <p style="text-align: left;">114</p>
                            </div>
                        </div>
                        <div class="form-group col-md-2 float-right">
                            <div class="row">
                                <p style="text-align: left;">JUMLAH AYAT</p>
                            </div>
                            <div class="row">
                                <p style="text-align: left;">6236</p>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="input-group md-form form-sm form-1 pl-3 pr-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text cyan lighten-2" id="basic-text1">
                                <i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                            <input class="form-control my-0 py-1" type="text" placeholder="Search" aria-label="Search">
                        </div>
                    </div>
                    <hr>
                </div>
                


                <div class="row pl-4">
                    <div class="form-group col-md-8 ">
                        <form class="form-inline pl-4 pr-0" style="background-color: cyan; height: 80px">
                            <div class="form-group">
                                <label class="sr-only" for="inputOne">Nomor Surat</label>
                                <input type="text" class="form-control" id="inputOne" placeholder="Nomor Surat">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="inputTwo">Nomor Ayat</label>
                                <input type="text" class="form-control" id="inputTwo" placeholder="Nomor Ayat">
                            </div>
                            <button type="submit" class="btn btn-default">Search</button>
                        </form>
                        <!-- <img class="img-responsive" src="<?php //echo base_url('assets/img/alfatihah.jpg');?>" style="width: 100%"> -->

                        <div class="borderquran" style="background-color: white;">
                            <p> وَیَٰۤأَیُّهَا ٱلَّذِینَ ءَامَنُوا۟ لَا تَتَّخِذُوا۟ عَدُوِّی وَعَدُوَّكُمۡ أَوۡلِیَاۤءَ تُلۡقُونَ إِلَیۡهِم بِٱلۡمَوَدَّةِ وَقَدۡ كَفَرُوا۟ بِمَا جَاۤءَكُم مِّنَ ٱلۡحَقِّ یُخۡرِجُونَ ٱلرَّسُولَ وَإِیَّاكُمۡ أَن تُؤۡمِنُوا۟ بِٱللَّهِ رَبِّكُمۡ إِن كُنتُمۡ خَرَجۡتُمۡ جِهَٰدࣰا فِی سَبِیلِی وَٱبۡتِغَاۤءَ مَرۡضَاتِیۚ تُسِرُّونَ إِلَیۡهِم بِٱلۡمَوَدَّةِ وَأَنَا۠ أَعۡلَمُ بِمَاۤ أَخۡفَیۡتُمۡ وَمَاۤ أَعۡلَنتُمۡۚ وَمَن یَفۡعَلۡهُ مِنكُمۡ فَقَدۡ ضَلَّ سَوَاۤءَ ٱلسَّبِیلِ ۝١
إِن یَثۡقَفُوكُمۡ یَكُونُوا۟ لَكُمۡ أَعۡدَاۤءࣰ وَیَبۡسُطُوۤا۟ إِلَیۡكُمۡ أَیۡدِیَهُمۡ وَأَلۡسِنَتَهُم بِٱلسُّوۤءِ وَوَدُّوا۟ لَوۡ تَكۡفُرُونَ ۝٢
لَن تَنفَعَكُمۡ أَرۡحَامُكُمۡ وَلَاۤ أَوۡلَٰدُكُمۡۚ یَوۡمَ ٱلۡقِیَٰمَةِ یَفۡصِلُ بَیۡنَكُمۡۚ وَٱللَّهُ بِمَا تَعۡمَلُونَ بَصِیرࣱ ۝٣
قَدۡ كَانَتۡ لَكُمۡ أُسۡوَةٌ حَسَنَةࣱ فِیۤ إِبۡرَٰهِیمَ وَٱلَّذِینَ مَعَهُۥۤ إِذۡ قَالُوا۟ لِقَوۡمِهِمۡ إِنَّا بُرَءَٰۤؤُا۟ مِنكُمۡ وَمِمَّا تَعۡبُدُونَ مِن دُونِ ٱللَّهِ كَفَرۡنَا بِكُمۡ وَبَدَا بَیۡنَنَا وَبَیۡنَكُمُ ٱلۡعَدَٰوَةُ وَٱلۡبَغۡضَاۤءُ أَبَدًا حَتَّىٰ تُؤۡمِنُوا۟ بِٱللَّهِ وَحۡدَهُۥۤ إِلَّا قَوۡلَ إِبۡرَٰهِیمَ لِأَبِیهِ لَأَسۡتَغۡفِرَنَّ لَكَ وَمَاۤ أَمۡلِكُ لَكَ مِنَ ٱللَّهِ مِن شَیۡءࣲۖ رَّبَّنَا عَلَیۡكَ تَوَكَّلۡنَا وَإِلَیۡكَ أَنَبۡنَا وَإِلَیۡكَ ٱلۡمَصِیرُ ۝٤
رَبَّنَا لَا تَجۡعَلۡنَا فِتۡنَةࣰ لِّلَّذِینَ كَفَرُوا۟ وَٱغۡفِرۡ لَنَا رَبَّنَاۤۖ إِنَّكَ أَنتَ ٱلۡعَزِیزُ ٱلۡحَكِیمُ ۝٥
لَقَدۡ كَانَ لَكُمۡ فِیهِمۡ أُسۡوَةٌ حَسَنَةࣱ لِّمَن كَانَ یَرۡجُوا۟ ٱللَّهَ وَٱلۡیَوۡمَ ٱلۡءَاخِرَۚ وَمَن یَتَوَلَّ فَإِنَّ ٱللَّهَ هُوَ ٱلۡغَنِیُّ ٱلۡحَمِیدُ ۝٦
۞ عَسَى ٱللَّهُ أَن یَجۡعَلَ بَیۡنَكُمۡ وَبَیۡنَ ٱلَّذِینَ عَادَیۡتُم مِّنۡهُم مَّوَدَّةࣰۚ وَٱللَّهُ قَدِیرࣱۚ وَٱللَّهُ غَفُورࣱ رَّحِیمࣱ ۝٧
لَّا یَنۡهَىٰكُمُ ٱللَّهُ عَنِ ٱلَّذِینَ لَمۡ یُقَٰتِلُوكُمۡ فِی ٱلدِّینِ وَلَمۡ یُخۡرِجُوكُم مِّن دِیَٰرِكُمۡ أَن تَبَرُّوهُمۡ وَتُقۡسِطُوۤا۟ إِلَیۡهِمۡۚ إِنَّ ٱللَّهَ یُحِبُّ ٱلۡمُقۡسِطِینَ ۝٨
إِنَّمَا یَنۡهَىٰكُمُ ٱللَّهُ عَنِ ٱلَّذِینَ قَٰتَلُوكُمۡ فِی ٱلدِّینِ وَأَخۡرَجُوكُم مِّن دِیَٰرِكُمۡ وَظَٰهَرُوا۟ عَلَىٰۤ إِخۡرَاجِكُمۡ أَن تَوَلَّوۡهُمۡۚ وَمَن یَتَوَلَّهُمۡ فَأُو۟لَٰۤئِكَ هُمُ ٱلظَّٰلِمُونَ ۝٩
یَٰۤأَیُّهَا ٱلَّذِینَ ءَامَنُوۤا۟ إِذَا جَاۤءَكُمُ ٱلۡمُؤۡمِنَٰتُ مُهَٰجِرَٰتࣲ فَٱمۡتَحِنُوهُنَّۖ ٱللَّهُ أَعۡلَمُ بِإِیمَٰنِهِنَّۖ فَإِنۡ عَلِمۡتُمُوهُنَّ مُؤۡمِنَٰتࣲ فَلَا تَرۡجِعُوهُنَّ إِلَى ٱلۡكُفَّارِۖ لَا هُنَّ حِلࣱّ لَّهُمۡ وَلَا هُمۡ یَحِلُّونَ لَهُنَّۖ وَءَاتُوهُم مَّاۤ أَنفَقُوا۟ۚ وَلَا جُنَاحَ عَلَیۡكُمۡ أَن تَنكِحُوهُنَّ إِذَاۤ ءَاتَیۡتُمُوهُنَّ أُجُورَهُنَّۚ وَلَا تُمۡسِكُوا۟ بِعِصَمِ ٱلۡكَوَافِرِ وَسۡءَلُوا۟ مَاۤ أَنفَقۡتُمۡ وَلۡیَسۡءَلُوا۟ مَاۤ أَنفَقُوا۟ۚ ذَٰلِكُمۡ حُكۡمُ ٱللَّهِ یَحۡكُمُ بَیۡنَكُمۡۖ وَٱللَّهُ عَلِیمٌ حَكِیمࣱ ۝١٠
وَإِن فَاتَكُمۡ شَیۡءࣱ مِّنۡ أَزۡوَٰجِكُمۡ إِلَى ٱلۡكُفَّارِ فَعَاقَبۡتُمۡ فَءَاتُوا۟ ٱلَّذِینَ ذَهَبَتۡ أَزۡوَٰجُهُم مِّثۡلَ مَاۤ أَنفَقُوا۟ۚ وَٱتَّقُوا۟ ٱللَّهَ ٱلَّذِیۤ أَنتُم بِهِۦ مُؤۡمِنُونَ ۝١١
یَٰۤأَیُّهَا ٱلنَّبِیُّ إِذَا جَاۤءَكَ ٱلۡمُؤۡمِنَٰتُ یُبَایِعۡنَكَ عَلَىٰۤ أَن لَّا یُشۡرِكۡنَ بِٱللَّهِ شَیۡءࣰا وَلَا یَسۡرِقۡنَ وَلَا یَزۡنِینَ وَلَا یَقۡتُلۡنَ أَوۡلَٰدَهُنَّ وَلَا یَأۡتِینَ بِبُهۡتَٰنࣲ یَفۡتَرِینَهُۥ بَیۡنَ أَیۡدِیهِنَّ وَأَرۡجُلِهِنَّ وَلَا یَعۡصِینَكَ فِی مَعۡرُوفࣲ فَبَایِعۡهُنَّ وَٱسۡتَغۡفِرۡ لَهُنَّ ٱللَّهَۚ إِنَّ ٱللَّهَ غَفُورࣱ رَّحِیمࣱ ۝١٢
یَٰۤأَیُّهَا ٱلَّذِینَ ءَامَنُوا۟ لَا تَتَوَلَّوۡا۟ قَوۡمًا غَضِبَ ٱللَّهُ عَلَیۡهِمۡ قَدۡ یَئِسُوا۟ مِنَ ٱلۡءَاخِرَةِ كَمَا یَئِسَ ٱلۡكُفَّارُ مِنۡ أَصۡحَٰبِ ٱلۡقُبُورِ ۝١٣

</p>

                        </div> 

                    </div>
                    <hr>

                    <div class="form-group col-md-4 pr-4 container-fluid">
                        <div class="panel panel-default bg-success">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="#" class="pull-left avatar">
                                            <img src="<?php echo base_url('assets/img/logo.png');?>" class="img-circle">
                                        </a>
                                    </div>
                                    <div class="col-md-9">
                                        <div style="margin-left: 5px; text-align: center;">
                                            <strong class="font-16">Alquran-ittp.com</strong>
                                            <br>
                                            <span class="grey">Situs Alquran</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group">
                                <a class="list-group-item">
                                    <i class="fa fa-bullhorn"></i>
                                    <span style="margin-left:45px; ">&#40;HR&#46; Ahmad&#41;</span>
                                </a>
                                <a class="list-group-item">
                                    <span style="text-align: center"><center>  Tamim Ad Dary radhiyalahu ‘anhu berkata: &#34;Rasulullah shallallahu &#39;alaihi wasallam bersabda&#58; &#40;Siapa yang membaca 100 ayat pada suatu malam dituliskan baginya pahala shalat sepanjang malam&#46;&#34; </center></span>
                                </a>
                            </div>
                        </div>
                        
                        <div class="panel panel-default bg-success">
                            <div class="panel-body">
                                 <span style="margin-left:45px; ">
                                    <h5>Juz Dalam Quran</h5>
                                </span>
                            </div>
                            <div class="list-group">
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[1]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[2]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[3]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[4]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[5]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[6]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[7]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[8]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[9]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[10]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[11]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[12]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[13]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[14]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[15]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[16]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[17]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[18]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[19]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[20]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[21]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[22]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[23]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[24]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[25]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[26]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[27]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[28]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[29]; ?></span>
                                </a>
                                <a class="list-group-item" href="#">
                                    <i class="fas fa-quran"></i>
                                    <span class="menu-text"><?php echo $juz[30]; ?></span>
                                </a>
                                
                            </div>
                        </div>


                    </div>

                    <hr>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <a href="https://github.com/azouaoui-med/pro-sidebar-template" class="btn btn-dark" target="_blank">
                                <i class="fab fa-github"></i> View source</a>
                            <a href="https://github.com/azouaoui-med/pro-sidebar-template/archive/gh-pages.zip" class="btn btn-outline-dark" target="_blank">
                                <i class="fa fa-download"></i> Download</a>
                        </div>
                    </div>
                    <hr>
                </div>
            </main>
            <!-- page-content" -->
        
        <!-- page-wrapper -->

        <!-- using online scripts 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
            
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"crossorigin="anonymous"></script>
            
        <!--<script src="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- using local scripts -->
        <script src="<?php echo base_url('/assets/js/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/js/popper.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/js/bootstrap.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/js/jquery.mCustomScrollbar.concat.min.js');?>"></script>

        <script src="<?php echo base_url('assets/js/main.js');?>"></script>
    </body>

    </html>